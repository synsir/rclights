#include <synos.h>

//#include "../stmbase/drivers/tm1638.h"
//#include "../stmbase/drivers/nrf24/rf24node.h"
//#include "../stmbase/drivers/ds18b20/ds18b20.h"
#include "../stmbase/drivers/sbus/sbus.h"

#include <stdio.h>
//#define printf(x, y) (x, y)
//#define printf(x) (x)

syn::Led alive_led;
SbusReader sbusreader;
//volatile uint16_t channel_1 = 0;
volatile uint16_t channel_2 = 0;
volatile uint16_t channel_3 = 0;
volatile uint16_t channel_4 = 0;


void check_sbus_routine(uint16_t arg)
{
  // uses port b5 (I2C)
  alive_led.init();
  alive_led.set();
  sbusreader.start();

  syn::Adc::turnOn();
  syn::Adc::initContinuous8(5);
  syn::Adc::convert(); // start continous conversion

  uint8_t no_frame_count = 0;
  while(1)
  {
    if(sbusreader.available())
    {
      alive_led.toggle();
      no_frame_count = 0;
      //channel_1 = sbusreader.channel_1();
      channel_2 = sbusreader.channel_2();
      //channel_3 = sbusreader.channel_3();
      channel_4 = sbusreader.channel_4();
      //set_pwm_out();
    }
    else
    {
      if(++no_frame_count >= 120)
      {
        no_frame_count = 0;
        alive_led.toggle();
      }
    }
    syn::Routine::sleep(5);
  }
}



#define BREAK_FULL 255
#define BREAK_IDLE 10
#define LIGHT_ON_THRESHHOLD 1024

uint8_t flood_target = 0;
uint8_t flood_current = 0;
bool flood_target_reached = true;
bool transmission_state = false;
uint8_t transmission_changed_counter = 0;

// front -> tim1_ch3 -> con 1 purple
// break -> tim1_ch4 -> con 2 grey
// flood -> tim1_ch1 -> con 4 black
// reverse -> tim2_ch1 -> con 3 -> white

void set_front(uint8_t val)
{
  syn::Timer1::setPWM(val, 3);
}

void set_break(uint8_t val)
{
  syn::Timer1::setPWM(val, 4);
}

void set_rev(uint8_t val)
{
  syn::Timer2::setPWM(val, 1);
}

void set_flood(uint8_t val)
{
  syn::Timer1::setPWM(val, 1);
}

void backlight_alarm()
{
  set_rev(255);
  set_break(0);
  syn::Routine::sleep(100);
  set_rev(0);
  set_break(255);
  syn::Routine::sleep(100);
}

void wait_for_signal()
{
  syn::Routine::sleep(100);
  while(sbusreader.failsafe())
  {
    backlight_alarm();
  }
  // my receiver doesnt put out the failsafe bit
  // while(channel_2 == 0)
  // this doesnt work well with actual futaba receivers that have the failsafe.
  // better not use!
}

void check_battery_level()
{
  // the divider used is 4k7 + 8k2
  // volatge goes down to 36% of the input
  // the speed controller puts out battery - 0.7V
  uint8_t bat = syn::Adc::read8();
  // about 2 Volt
  if(bat < 152)
  {
    backlight_alarm();
  }
  // so the trigger voltage would be
  // 2V * (1 / 0.36) + 0.7V = 6.2 Volt. very low
}

void startup_lights()
{
  for(uint8_t i = 0; i < 255; ++i)
  {
    set_front(i);
    syn::Routine::sleep(20);
  }
}

bool gas_is_idle()
{
  // neutral = 975
  return channel_2 >= 885 && channel_2 <= 1070;
}

bool gas_is_reverse()
{
  return channel_2 <= 885;
}

void set_flood_on()
{
  flood_current = 0;
  flood_target = 255;
  flood_target_reached = false;
}

void set_flood_off()
{
  flood_current = 25;
  flood_target = 0;
  flood_target_reached = false;
}

bool check_switch_pressed()
{
  if(channel_4 < 1300)
  {
    // transmission state and channel value dont match
    ++transmission_changed_counter;
    if(transmission_changed_counter > 50)
    {
      // reached value over 50, aka 500ms in this gear, dont switch lights
      transmission_changed_counter = 0;
      transmission_state = !transmission_state;
    }
  }
  else
  {
    if(transmission_changed_counter > 5)
    {
      // recently quickly switched transmission, indicate this to caller
      transmission_changed_counter = 0;
      return true;
    }
  }
  return false;
}

bool check_flood_on_off()
{
  return channel_4 < 1300;
}

void set_flood_lights()
{
  if(flood_target_reached)
  {
    if(check_flood_on_off())
    {
      if(flood_current == 0)
      {
        set_flood_on();
      }
      else
      {
        set_flood_off();
      }
    }
  }
  else
  {
    if(flood_current < flood_target)
    {
      ++flood_current;
    }
    else
    {
      --flood_current;
    }
    set_flood(flood_current);
    if(flood_current == flood_target)
    {
      flood_target_reached = true;
    }
  }
}


void set_lights_routine(uint16_t arg)
{
  // front -> tim1_ch3 -> con 1 purple
  // break -> tim1_ch4 -> con 2 grey
  // flood -> tim1_ch1 -> con 4 black
  syn::Timer1::init_arduinopwm();
  syn::Timer1::enablePWM(0xD);
  // reverse -> tim2_ch1 -> con 3 -> white
  syn::Timer2::init_arduinopwm();
  syn::Timer2::enablePWM(0x1);

  set_front(0);
  set_break(BREAK_FULL);
  set_flood(0);
  // wait for first signal to arrive
  wait_for_signal();
  // start front lights
  startup_lights();
  // run regular loop
  while (1)
  {
    check_battery_level();
    if(gas_is_idle())
    {
      set_rev(0);
      set_break(BREAK_FULL);
    }
    else if (gas_is_reverse())
    {
      set_rev(255);
      set_break(BREAK_IDLE);
    }
    else
    {
      set_rev(0);
      set_break(BREAK_IDLE);
    }
    set_flood_lights();
    syn::Routine::sleep(10);
  }
}


int main()
{
  syn::Kernel::init();

  // IMPORTANT !!!
  // the count of routines has to exactly match the amount of routines used
  // the routine index given during initialization has to match

  // Routines should never return, but there is a safeguard if that happens.
  // However that safeguard costs 2 bytes of stack and 6 bytes of rom.
  // furthermore, if roundrobin is disabled, the processor will be stuck on the
  // returned rotuine forever. you have been warned.


  // stack allocated automatically
  syn::Routine::init(&check_sbus_routine, 0, 190);

  syn::Routine::init(&set_lights_routine, 0, 190);

  //syn::SysTickHook::init<0, 10>(tim_x);
  //syn::SysTickHook::init<1, 10>(tim_y);

  // never returns
  syn::Kernel::spin();
  return 0;
}
